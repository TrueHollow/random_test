#include <cstdint>
#include <iostream>
#include <atomic>
#include <vector>
#include <thread>
#include <stdio.h>
#include <string>

static std::atomic<uint64_t> iteration_count(0);
volatile static std::atomic<bool> stop_flag(false);
static std::atomic<bool> found(false);

static size_t same_symbols_count;
static size_t variation_count(10);
std::vector<std::thread> tasks;
static std::vector<std::atomic<uint64_t>> counters;

void strFormat(std::string &str) {
	const auto strLength = str.length();
	for (auto i = strLength - 3; i > 0 && i < strLength; i -= 3) {
		str.insert(i, ",");
	}
}

void printResult() {
	for (size_t i = 1; i < counters.size(); ++i)
	{
		std::cout << "Type: " << i << ", value: " << counters[i] << std::endl;
	}
}

void task() {
    const auto thread_id = std::this_thread::get_id();
    std::cout << "Task " << thread_id << " is started." << std::endl;
    srand (time(nullptr));
    int val;
	auto current_val = -1;
    size_t same_count = 0;
    do {
        ++iteration_count;
        val = rand() % variation_count;
        if (current_val != val) {
            current_val = val;
            same_count = 1;
        } else {
            ++same_count;
            if (same_count >= same_symbols_count) {
                stop_flag = true;
                found = true;
				auto str = std::to_string(iteration_count);
				std::this_thread::sleep_for(std::chrono::seconds(1));
				strFormat(str);
				std::cout << "Task " << thread_id << " fount value " << val << " repeated " << same_count << " on iteration " << str << std::endl;
				printResult();
                break;
            }
        }
		++counters[same_count];
    } while(stop_flag == false);
    std::cout << "Task " << thread_id << " is stopped." <<  std::endl;
}

int main(int argc, char *argv[])
{
    char ch;
    same_symbols_count = 9;
    size_t task_count;

    std::cout << "Start finding!" << std::endl;
    do {
        std::cout << "Using multicore(Y/N)? ";
        std::cin >> ch;
        std::cout << std::endl;
    } while (ch != 'Y' && ch != 'y' && ch != 'N' && ch != 'n');

    if (ch == 'N' || ch == 'n') {
        task_count = 1;
    } else {
        task_count = std::thread::hardware_concurrency();
        if (task_count == 0) {
            task_count = 2;
        }
    }

	counters = std::vector<std::atomic<uint64_t>>(same_symbols_count + 1);

    std::cout << "Running " << task_count << std::endl;

    tasks.reserve(task_count);
    for (size_t i = 0; i < task_count; ++i) {
        tasks.push_back(std::thread(task));
    }

	std::this_thread::sleep_for(std::chrono::seconds(1));

	while (true)
	{
        std::cout << "Press any key to abort " << std::endl;
        std::cin >> ch;
        std::cout << std::endl;
		break;
    }
    stop_flag = true;

    for (auto& thread : tasks) {
        thread.join();
    }

    std::cout << (found ? "Found " : "Not fount ") << "in " << iteration_count << std::endl;
	std::cin >> ch;
    return 0;
}
