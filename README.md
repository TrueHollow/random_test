# README

## Оглавление:
---

[TOC]

---

## Компиляция:
### Сборка под Windows:
1. Использовать [CMake](https://cmake.org/) (тестировал на [генераторах](https://cmake.org/cmake/help/latest/manual/cmake-generators.7.html) **NMake Makefiles**, **Visual Studio 14 2015**).

Пример №1:

```
#!cmd
> mkdir build32 & cd build32
> cmake -G "Visual Studio 14 2015" %PATH_TO_SOURCES%
> cd ..
> mkdir build64 & cd build64
> cmake -G "Visual Studio 14 2015 Win64" %PATH_TO_SOURCES%
> cd ..
> cmake --build build32 --config Release
> cmake --build build64 --config Release
```

Пример №2:

```
#!cmd
> cmake %PATH_TO_SOURCES% -Bbuild32 -G "Visual Studio 14 2015"
> cmake --build build32 --config Release
> cmake %PATH_TO_SOURCES% -Bbuild64 -G "Visual Studio 14 2015 Win64"
> cmake --build build64 --config Release
```

### Сборка под Linux:
1. Использовать [CMake](https://cmake.org/) (тестировал на [генераторах](https://cmake.org/cmake/help/latest/manual/cmake-generators.7.html) **Unix Makefiles**, **Ninja**).

Пример №1:

```
$ cmake -G "Unix Makefiles" -D CMAKE_BUILD_TYPE=$BUILD_TYPE $PATH_TO_SOURCES # сборка автоматически использует системную разрядность (x86, x64), $BUILD_TYPE = Debug|Release|RelWithDebInfo|MinSizeRel.
$ cmake --build build -- VERBOSE=1 -j 8 # где 8 число ядер процессора. Опция ускоряет компиляцию с использованием make, для ninja не требуется
```